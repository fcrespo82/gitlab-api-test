# frozen_string_literal: true

def find_endpoints(file)
  re = /^(?<method>GET|PUT|POST|DELETE).*?  # method
(?<path>\/.+?)$.+?^                       # Path
(?<table>(?<header>                       # header capture
  ^                                       # line start
  (?:[^\r\n]*?\|[^\r\n]*)                 # line w\/ at least one pipe
  $                                       # line end
  (?:\r?\n)                               # newline
)?                                        # optional header
(?<format>                                # format capture
  ^                                       # line start
  (?:[ :-]*?\|[ :-]*)+                    # line containing separator items w\/ at least one pipe
  \s*                                     # maybe trailing whitespace
  $                                       # line end
  (?:\r?\n)?                              # newline
)
(?<body>                                  # body capture
  (?:
    (?:[^\r\n]*?\|[^\r\n]*)               # line w\/ at least one pipe
    \s*?                                  # maybe trailing whitespace
    (?:\r?\n|^|$)?                        # newline
  )+                                      # at least one
))/mx

  file = File.open(file)
  file_data = file.readlines.join
  matches = []
  file_data.scan(re) { matches << Regexp.last_match }
  matches
end

def extract_parameters(table)
  re = /^.+?(?<attribute>\w+).+?\|.+?(?<type>\w+).+?\|.+?(?<required>\w+).+?\|.+?(?<description>.+).+?\|$/mx
  lines = table.split("\n").drop(2)
  matches = []
  lines.each do |line|
    line.scan(re) { matches << Regexp.last_match }
  end
  matches
end

def generate_block(endpoint, parameters)
  method = endpoint.named_captures["method"]

  paramsdoc = ""
  parameters.each do |parameter|
    paramsdoc += %(
    - in: query
      name: #{parameter.named_captures["attribute"]}
      schema:
        type: #{parameter.named_captures["type"]}
      required: #{parameter.named_captures["required"] == "no" ? "false" : "true"}
      description: "#{parameter.named_captures["description"]}")
  end

  %("#{method.downcase}:
  parameters:#{paramsdoc}")
end

def main
  project_endpoints = find_endpoints("test/projects.md")
  endpoint = project_endpoints[0]
  table = endpoint.named_captures["table"]
  parameters = extract_parameters(table)
  puts generate_block(endpoint, parameters)
end

main
